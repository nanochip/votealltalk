# [TF2] Votealltalk



## Description:
A voting system that toggles the status of Alltalk and utilizes [NativeVotes](https://github.com/nosoop/sourcemod-nativevotes).

## Dependencies
* **[NativeVotes](https://github.com/nosoop/sourcemod-nativevotes)**

## CVARS:
This plugin will auto generate a cvar config once the plugin is loaded in *tf/cfg/sourcemod/plugin.votealltalk.cfg*
* **votealltalk_time [#.#]** - (Default: 20.0) Time in seconds the vote menu should last.
* **votealltalk_delay [#.#]** - (Default: 120.0) Time in seconds before players can initiate another alltalk vote.
* **votealltalk_chat_percentage [#.#]** - (Default: 0.30) How many players are required for the chat vote to pass? 0.30 = 30%.
* **votealltalk_menu_percentage [#.#]** - (Default: 0.70) How many players are required for the menu vote to pass? 0.70 = 70%.

## Commands:
* **sm_votealltalk** or chat say **votealltalk** - (Default Access: Everyone) Vote to scramble the teams.
* **sm_valltalk** or chat say **valltalk** - (Default Access: Everyone) Vote to scramble the teams.
* **sm_alltalk** - (Default Access: Everyone) See the current status of Alltalk.
* **sm_forcealltalk** - (Default Access: ADMFLAG_VOTE) Force an Alltalk menu vote.

## Installation:
1. [Install Sourcemod](https://wiki.alliedmods.net/Installing_sourcemod).
2. Install [NativeVotes](https://github.com/nosoop/sourcemod-nativevotes) by extracting the addons/ folder to your tf/ folder.
3. Download this repository and extract the addons/ folder to your tf/ folder.
4. Restart the server or type "sm plugins load votealltalk" in your server console.
5. Configure the plugin in *tf/cfg/sourcemod/plugin.votealltalk.cfg* to your liking.

## To-Do List:
I am always welcome to suggestions!
* none